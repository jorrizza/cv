% Curriculum Vitae
% Joris van Rooij, Staff Engineer
% __NOW__

---------------------------------------

## Professional Skills ##

A software engineer by trade with a couple of decades of experience. Designing,
building, maintaining, and deploying software systems. Delivery management,
technical leadership, knowledge sharing. A strong focus on developer
experience.

## Work Experience ##

### *2021-now:* Staff Engineer at *Sendcloud* ###

Started a DevOps team from scratch to introduce the mindset to the engineering
teams at Sendcloud by means of education, bespoke tooling, and automation. The
end goal is to no longer need a DevOps team, enabling all engineers to
independently develop and maintain their software in production using a
self-service platform on top of Amazon Web Services and Kubernetes.

### *2018-2021:* Lead Developer and Software Architect at *Jibe Company* ###

As a team lead, working on several projects for a number of large,
international organizations including the European Commission, ASML, and
TP-Vision. These projects range from blockchain-based data security projects,
to electric vehicle charging grid management, to embedded software. Often
working with experts across Europe. The work at Jibe Company mainly focused on
software architecture, operational security, and scalabilty on cloud
infrastructure.

### *2005-now:* Co-founder at *wasda* ###

Initially, Linux/Unix enthusiasts sharing rack space and a common domain name.
Since 2019 incorporated as a company with a focus on research and product
development in the software development sector using every bit of cool tech
under the sun. This is where the innovation happens in my free time.

### *2013-2023:* Co-founder and DevOps Connaisseur at *Constructors* ###

A group of like-minded individuals forming a full service web engineering
bureau. I was mainly responsible for keeping the applications running smoothly
on our in-house cloud hosting platform. It includes automatic deployment,
monitoring and analytics of mission critical applications in many countries for
a wide range of customers. Sometimes I could also be seen doing some Ruby on
Rails or Django web development work.

### *2014-2018:* Software Engineer at *ViaViela* ###

Mainly software development and day-to-day IT operations for one of the
Netherlands's largest childcare organizations. Continued my work here after
Mediaklapper came to an end. During this time parts of the applications were
moved from in-house hosting facilities to cloud providers like Amazon Web
Services.

### *2010-2014:* Co-founder at *Mediaklapper* ###

Mediaklapper was a VC funded start-up in advertising. Managed and executed the
construction of a development and hosting platform, hardware and all, for our
in-house Ruby on Rails application. Main components included Debian, Nginx,
Ruby, HAProxy, Icinga, and PostgreSQL. I've also built parts of the
application, including a search engine.

### *2010-2014:* IT Consultant at *ViaViela* ###

This was a part-time arrangement while working at Mediaklapper. Kept the
*e-Viela!* stack running. Managed a large VPN (OpenBSD, Tinc) for remote
desktops (NX), telephony (Asterisk), and hosting. Did some software development
as well. Mainly Ruby on Rails and PHP.

### *2006-2010:* Software Engineer at *e-Viela!* ###

F/LOSS based software solutions for medium to big sized organizations. The work
was mostly done using the LAMP (Linux Apache MySQL PHP) stack, for which I also
built and maintained the bare metal hosting facilities. I've helped introduce
Linux desktops (NX) into the company. This company's activities have been
merged into *ViaViela* in 2010.

### *2004-2008:* Software Engineer at *Eurovision Multimedia* ###

As a teenager, made them a web shop which integrated with existing DOS-based
sales software using Linux, Apache, MySQL, and PHP. The web shop was so
successful they closed the brick and mortar store.

## Education and Qualifications ##

### *2005-2010:* Bachelor of ICT ###

Embedded Systems, Fontys University of Applied Sciences, Eindhoven

### *1999-2005:* HAVO ###

Dr. Knippenbergcollege, Helmond

## Technologies ##

The following section is keyword stuffing for search engines. It's not an
exhaustive list, but it covers a large chunk. Technologies are mere tools and
do not, in my opinion, define a person's skills.

### Languages ###

Dutch (native), Brabantish (fluent), English (fluent)

### Programming Languages ###

Currently mainly using the following:

C, Zig, Go, Ruby, Python, Bash/sh, SQL

But can occasionally seen dabbling with:

Emacs Lisp, Lua, JavaScript, Awk, C++, Java, Rust, Kotlin

### Cloud ###

Kubernetes, Amazon Web Services, Hetzner Cloud

### Operating Systems ###

(mostly Debian) Linux, OpenBSD, FreeBSD

### Tooling ###

Other tools I regularly work with:

RabbitMQ, GitLab, Docker, Ansible, Terraform, Prometheus, Grafana, Datadog,
Helm, Argo CD

### Networking ###

Network topologies and design, VPNs (Tinc, OpenVPN, Wireguard), routing and
firewalling using *BSD and/or Linux, DNS, DHCP, PXE, TCP/IP and several other
related cool-sounding acronyms.

### Databases ###

MySQL and PostgreSQL, both in clustered setups. MongoDB and Elasticsearch.
Redis and memcached. SQLite and bbolt embedded databases. Even a little
Clickhouse.

### Project Management ###

Agile/Scrum, XP, Kanban, Waterfall.

## About ##

Starting out with an old Philips MSX II as a kid, and writing software ever
since. Started using Linux in 2000 and haven't looked back. A fierce advocate
of open-source software, digital rights, and operational security. Interested
in everything software-related, from architecture and development, to
operations and availability. Definitely a soft spot for well-designed
distributed systems, both technical and organizational.

My leading philosophy is _keep it simple_. Through that I try to (re-)ignite
the love for software craftsmanship in both myself and others. To spread the
word I [write articles](https://wasda.dev/) and occasionally give talks.

## Hobbies and Interests ##

Motorcycle traveling, (political) philosophy, civil rights, cryptography,
obscure computer hardware, and hounds.

## Personal Details ##

* *name:* Joris van Rooij
* *place of residence:* Son, the Netherlands
* *citizenship:* Dutch
* *year of birth:* 1987
* *email address:* cv@jrrzz.net
* *PGP key*: [C842 7316 51DA 8B7C 9F34 B546 7B07 42A5 E4CB 4D57](https://www.jrrzz.net/pubkey.txt)
